[TOC]
## 插值 {{}} 、{html{}} 
av.js 允许采用简洁的模板语法来声明式地将数据渲染进 DOM 的系统。
```
<div>{{ message }}</div>
```
工程的渲染数据如下：
```
av({
    id:'index',
    selector:'#content',
    export : {template: "test.html"},
    import : function(e){
        this.template(e.template);
    },
    data: {
        message:"hello av.js!",
        htmlTest:"<span>hello av.js!</span>",
    },
});
```
最后渲染为：
```
<div>hello av.js!</div>
```
渲染为HTML节点插值：
```
<div>{html{htmlTest}}</div>
```
使用 JavaScript 表达式：
```
<div>{{ message + '测试' }}</div>
```
## 模板 av-template
该命令用于插入模板字符串。该命令不能在同一个标签中同时存在多个。
该命令会根据模板字符串创建子节点树。
```
<div av-template="example"></div>
```
工程的渲染数据如下：
```
av({
    id:'index',
    selector:'#content',
    export : {template: "test.html",templateTemplate: "test.html"},
    import : function(e){
        this.template(e.template);
        //将模板数据赋给渲染数据
        this.data.example = e.templateTemplate;
    },
    data: {
        example:"",
    },
});
```
## 遍历 av-for
该命令用于根据对象、数组来遍历性地渲染一块内容。该命令不能在同一个标签中同时存在多个。
在下面的命令中`av-for="(item, index)items"`，第一个参数`item`是循环单位value的别名；第二个参数`index`是循环单位key的别名并且可以省略。两个参数以空格隔开。
```
<ul id="example">
  <li av-for="(item, index) items">
    {{ item.message }}
  </li>
</ul>
```
被遍历变量也可以是一个函数，可以传入函数参数。但这个函数的返回值必须是Array类型或者Object类型的数据：
```
<ul id="example" av-for="(item , index) itemsFunction()">
  <li av-for="(sonItem) item.son(type,'123')">
    {{ sonItem.message }}
  </li>
</ul>
```
## 分支 av-if、av-else-if、av-elseif、av-else
该组命令用于条件性地渲染一块内容。这块内容只会在命令的表达式返回 truthy 值的时候被渲染。该组命令不能在同一个标签中同时存在或存在多个。
av-if 指令用于条件性地渲染一块内容。这块内容只会在指令的表达式返回 truthy 值的时候被渲染。
av-else 延伸了 av-if 语句，可以在 av-if 语句中的表达式的值为 falsy 时执行语句。
av-else-if，和此名称暗示的一样，是 av-if 和 av-else 的组合。和 av-else 一样，它延伸了 av-if 语句，可以在原来的 av-if 表达式值为 falsy 时执行不同语句。但是和 av-else 不一样的是，它仅在 av-else-if 的条件表达式值为 truthy 时执行语句。注意，av-elseif 只是 av-else-if的简写，两者是效果一样的。
```
<div av-if="typeof testArray == 'object'">testArray 是一个对象</div>
条件中间是可以写文本和注释的
<!--av-elseif 和 av-else-if 是效果一样的-->
<div av-elseif="testArray">
    testArray 不是数组，但是一个真值
</div>
<div av-else>
    testArray 为假
</div>
```
注意，truthy 不是 `true`，详见 [MDN](https://developer.mozilla.org/zh-CN/docs/Glossary/Truthy) 的解释。falsy 也不是 `false`，详见 [MDN](https://developer.mozilla.org/zh-CN/docs/Glossary/Falsy) 的解释。

## 条件 av-is
该命令与av-if一样，用于条件性地渲染一块内容。这块内容只会在命令的表达式返回 truthy 值的时候被渲染。该命令不能在同一个标签中存在多个。
如果同时使用了av-if、av-is这两个命令时，需要两个命令值都为真时才会去渲染数据。而其中av-if会先进行执行判断，然后再执行判断av-is。
注意，同一个节点中av-for命令相对于av-is具有优先级，所以av-is可以判断av-for遍历出来的参数值，不能判断items同级渲染数据。

```
<ul id="example">
  <li av-for="(item,index)items" av-is="item.id == '1'">
    {{ item.message }}
  </li>
</ul>
```
而av-if相对于av-for命令具有优先级，所以是无法在同一个节点上判断其遍历参数。只能判断items同级渲染数据。
```
<ul id="example">
  <li av-for="(item,index)items" av-if="typeof items == 'object'">
    {{ item.message }}
  </li>
</ul>
```
## 无效 av-void、&lt;av-void&gt;
该命令是将节点当做无效的包裹节点，最终的渲染结果将不包含其节点。
|  如果同一个标签存在其他的命令，下列命令才有效果  |  
| --- |
| av-template |
| av-for|
| av-if |
| av-else-if |
| av-else |
| av-is |
| av-print |
| av-print-html |
如果非上列命令，让其他命令与其搭配的话将无其他命令效果，因为av-void是具有优先级的。该命令有两种写法，一种是属性的方式，一种是标签的方式。
```
<ul id="example">
    <div av-void av-for="(item, index) items">
      <li>
        {{ item.message }}
      </li>
    </div>
</ul>
```
```
<ul id="example">
    <av-void av-for="(item, index) items">
      <li>
        {{ item.message }}
      </li>
    </av-void>
</ul>
```
用无效命令配合打印命令实现插值`{{}}`功能：
```
<av-void av-print="message"/>
```
上例等同于下例写法：
```
{{message}}
```
## 打印 av-text、av-html
该命令的表达式结果会作为字符串返回值来打印。该命令不能在同一个标签中存在多个。
注意，插值{{}}、{html{}}是打印av-text、av-html的别名写法，彼此效果一致。
打印普通文本，该命运会将html实体编码：
```
<div av-text="name"></div>
```
打印HTML标签，该命运对html实体不做任何处理：
```
<div av-html="name"></div>
```
如果将打印标签中存在子标签，那么打印永远会渲染到最后面，如下情况：
```
<div av-text="name"><span>这是测试原始子节点{{test}}</span>测试</div>
```
```
av({
    id:'index',
    selector:'#content',
    data: {
        name: 'NAME',
        test: 'TEST'
    },
});
```
渲染结果为：
```
<div><span>这是测试原始子节点TEST</span>测试NAME</div>
```

## 属性 av-attribute、av-attr
该命令的表达式结果返回字符串作为属性值。该命令在同一个标签中可以存在多个。
`av-attr` 是`av-attribute`的别名，语法为`av-attr="[属性名称] 值或者表达式"`，注意其中属性名称可以为多个，以空格隔开。
```
<div av-attr="[name] 'attrTestName' " av-attr="[id] attrTest">属性添加</div>
```

## 类 av-class
该命令的表达式结果的类型必须是对象。该命令不能在同一个标签中存在多个。
对象的键名称渲染出来即是类名称，这个类名称是否存在将取决于对应值的返回值是否为真。下面的语法表示 `active` 这个 class 存在与否将取决于渲染数据 `isActive` 是否为真：
```
<div av-class="{ active: isActive }"></div>
```
在对象中传入更多属性来动态切换多个 class。此外，`av-class` 指令也可以与普通的 class 属性共存。下面的语法表示`isStatic `为假，那么会删除`static`类。同理，当`isActive`、`hasError`为假，那么`active`与`text-danger`会被删除，反之则添加。
```
<div class="static" av-class="{ active: isActive, 'text-danger': hasError, static: isStatic }"></div>
```
如果你也想根据条件切换列表中的 class，可以用三元表达式：
```
<div av-class="{active:(isActive == 'test'?true:false)}"></div>
```
## 样式 av-style
该命令的表达式结果的类型必须是对象。该命令不能在同一个标签中存在多个。
CSS 属性名可以用驼峰式 (camelCase) 或短横线分隔 (kebab-case，记得用引号括起来) 来命名：
```
<div av-style="{ color: activeColor, fontSize: fontSize + 'px' }"></div>
```
在工程中的表现：
```
av({
    id:'index',
    selector:'#content',
    data: {
        activeColor: 'red',
        fontSize: 30
    },
});
```
可以为 `av-style` 绑定中的属性提供一个包含多个值的数组，常用于提供多个带前缀的值，例如：
```
<div av-style="{ display: ['-webkit-box', '-ms-flexbox', 'flex'] }"></div>
```
这样写只会渲染数组中最后一个被浏览器支持的值。
在本例中，如果浏览器支持不带浏览器前缀的 flexbox，那么就只会渲染： `display: flex`

有时候还可以根据渲染数据的真假值来删除样式，如下，当`v`为假则设为空字符串 ，便会取消掉该样式：
```
av-style="{'color': (v?'red':'') }"    
```

## 事件 av-event
该命令的表达式中可以多个事件，但必须配合一个函数。该命令在同一个标签中可以存在多个。
如下面语法中，`eClickFnTest`是一个函数，如果参数为空可以省略`()`符号。
语法格式：`av-event="[事件名称] 事件执行函数"`
```
<div av-event="[click] eClickFnTest">这是一个点击事件</div>
```
传入自定义参数：
```
<div av-event="[click] eClickFnTest(123,'abc',$val)">这是一个点击事件</div>
```
指定方法函数接收编译参数，需要加上`:`符号前缀，表示赋值给所在位置参数：
```
<div av-event="[click]  eClickFnTest(:node,:e)">这是一个点击事件</div>
```
指定方法函数接收编译参数并且传入自定义参数：
```
<div av-event="[click] eClickFnTest($val,:event,:n)">这是一个点击事件</div>
```
在同一个标签中可以存在多个：
```
<input value="" av-event="[click] eClickFnTest()" av-event="[input propertychange] eChangeFnTest(:node,:value)"/>
```
## 助手 av-helper
该命令的表达式中只能存在一个函数，但是该命令在同一个标签中可以存在多个。
助手是节点最后执行的语法，所以能获取节点有价值的参数。注意，每次渲染的时候，无论传入的参数是否发生改变都会执行助手函数。
如下面语法中，`methodFnTest`是一个函数，如果参数为空可以省略`()`符号。
语法格式：`av-helper="方法执行函数"`
```
<div av-helper="methodFnTest">这是方法测试</div>
```
在渲染过程中，该模板语法是最后才进行渲染的，所以可以在这一步进行节点取值，处理其他操作。传入自定义参数：
```
<div av-helper="methodFnTest(123,'abc',$val)">这是方法测试</div>
```
指定方法函数接收编译参数，需要加上`:`符号前缀，表示赋值给所在位置参数：
```
<div av-helper="methodFnTest(:node,:c)">这是方法测试</div>
```
指定方法函数接收编译参数并且传入自定义参数：
```
<div av-helper="methodFnTest($val,:event,:n)">这是方法测试</div>
```
在同一个标签中可以存在多个：
```
<input value="这是方法测试" av-helper="methodFnTest()" av-helper="methodFnTest(:node,:value)"/>
```
## 指定事件、助手函数接收编译参数
编译参数需要加上`:`符号前缀，不区分大小写：
```
<div av-helper="testFunction(:node,:a,:value,:E,:class,:style,:t)"></div>
```
|  参数标签   | 简写别名 |  支持命令   |  值类型 |描述   |
| --- | --- |--- |--- |--- |
|   event |   e | av-event/av-helper| Object | 获取当前事件对象 |
|   node  | n  | av-event/av-helper| HTMLElement  | 获取标签的节点对象 |
|   value |  v | av-event/av-helper| multiple | 获取节点的值，一般用于input、textarea、select等表单节点 |
|   template | t |  av-event/av-helper| String | 获取节点编译时的模板数据 |

## av-template、av-for、av-html、{html{}} 打印模板字符串的比较
av-template具有优先级，是最先进行渲染，并且av-template与av-for一样，在编译的时候不会编译内部内容。av-for 会根据遍历个数进行对内部文本进行编译和渲染，而av-template只认表达式所返回的模板字符串，其内部文本会被无视掉。av-html、{html{}}两者效果相同，但优先级小于 av-for ，其内部文本会及时编译并且始终显示在节点前面。av-template是不会强制渲染刷新(refresh)，除非模板字符发生变更，而av-html、{html{}}是可以强制渲染刷新的(refresh)。

