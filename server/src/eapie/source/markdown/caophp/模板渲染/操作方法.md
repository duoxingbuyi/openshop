<blockquote class="info"><p>模板方法是给模板变量赋值、模板渲染的操作。</p></blockquote>


##### 全局赋值函数：
~~~
template([mixed $key_or_config][,mixed $value][,bool $coverage = false][,closure $closure]);
~~~

##### 继承框架类：
~~~
framework\cao::template([mixed $key_or_config][,mixed $value][,bool $coverage = false][,closure $closure]);
~~~

| 参数类型  |  参数名称  |  参数备注  |   是否必须   |  传参顺序   |
| --- | --- | --- | --- | --- |
|mixed|$key_or_config|  模板赋值变量的键或者渲染配置|否|  mixed[0] |
|mixed|$value|  模板赋值变量的值|否|mixed[1] |
|mixed|$coverage|  是否覆盖已存在模板变量键|否|mixed[2] |
|  closure  |  $closure  |  闭包函数  |  否  | closure[0] |

- $key_or_config 如果是数组，那么这是渲染模板操作。如果是字符串，那么这是模板赋值操作。
- $closure 放入一个参数，如function($data){}，$data就是最后处理返回的数据。
- 如果是指定返回已定义的模板变量键信息，存在则已，不存在这个键返回NULL。
- 如果是定义模板变量键值，成功返回true，失败返回false。
- 如果在已经定义的模板变量键值，如果再定义，但是$coverage为false，那么不覆盖则定义失败，返回false。