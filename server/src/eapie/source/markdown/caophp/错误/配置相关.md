
```
 Array (
	//是否开启错误提示。true开启(默认)，false关闭。关闭错误后，有错误而不提示错误，只终止程序。
    [start] => 1
    //是否捕获系统错误。true开启，false关闭(默认)。开启时需要与cao::error()方法配合使用。
    [catch] => 
    
    //注意，catch具有优先级，如果catch开启，那么start则无效。
    
    //错误信息保存在运行缓存目录的文件夹名称。
    [cache_folder_name] => error
    //是否生成错误日记文件。true开启(默认)，false关闭。开启时错误信息会记录到运行缓存目录的错误日志文件中。
    [log_file] => 1
)
```
