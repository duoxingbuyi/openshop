<blockquote class="info"><p>配置的操作方法根据不同的参数，可以获取配置和设置配置。</p></blockquote>

##### 全局赋值函数：
~~~
config([mixed $key][,mixed $value][,closure $closure]);
~~~

##### 继承框架类：
~~~
framework\cao::config([mixed $key][,mixed $value][,closure $closure]);
~~~

| 参数类型  |  参数名称  |  参数备注  |  是否必须  |  传参顺序 |
| --- | --- | --- | --- | --- |
|  mixed  | $key  |  键  |  否  |  mixed[0]  |
|  mixed  | $value  |  值  |  否  | mixed[1] |
|  closure  | $closure  |  闭包函数  |  否  | closure[0] |

- 如果$key是一个布尔值，并且等于 true 那么代表基础配置有变化，需要更新基础配置。
- $closure 放入一个参数，如function($data){}，$db就是实例化后的对象。
- 在设置配置时，成功返回true，失败(比如不合法情况下)返回false
- 在指定键获取时，存在则已，不存在返回NULL