#### 初始化配置
初始化配置，也就是在引入框架时，在入口文件对配置的第一次自定义设置，该操作将会覆盖框架默认的配置。
~~~
//引入框架类文件
include 'framework/cao.php';
//初始配置信息
$config = array(
    'error'=>array(
            'start'=> true,
            'catch'=> false
    ),
    'db' => array(
		'method_log' => true,	//是否记录method执行信息。
		'query_log'=>true,	//是否记录query语句信息。
	),
    'template'	=> array(
        'html_cache_start' => false,	//是否开启html缓存。
    ),
    //......
);
//初始化项目
framework\cao::install($config, function(){
	//...
	});
~~~

* * * * *

#### 默认配置
配置信息是一个多维数组，下面是框架默认的全部配置信息：
~~~
Array
(
	/* 基础环境设置 */
    //设置客户端断开连接时是否中断脚本的执行。true 忽略与用户的断开(默认)，false 会导致脚本停止运行。
    [ignore_user_abort] => 1
    //设置程序执行时间，单位：秒。默认为0，表示无时间上的限制。
    [set_time_limit] => 0
    //字符集
    [charset] => utf-8
    //时区
    [timezone] => PRC
    //框架语言
    [framework_language] => zh_cn
    
    /* 错误信息配置 */
    [error] => Array
        (
        	//是否开启错误提示。true开启(默认)，false关闭。关闭错误后，有错误而不提示错误，只终止程序。
            [start] => 1
            //是否捕获系统错误。true开启，false关闭(默认)。开启时需要与cao::error()方法配合使用。
            [catch] => 
            //错误信息保存在运行缓存目录的文件夹名称。
            [cache_folder_name] => error
            //是否生成错误日记文件。true开启(默认)，false关闭。开启时错误信息会记录到运行缓存目录的错误日志文件中。
            [log_file] => 1
        )

	/* 模板配置 */
    [template] => Array
        (
        	//设置模板的绝对路径
            [path] => 
            //缓存方式。html，php(默认)。
            [cache] => php
            /*【只对html缓存模式有效】是否生成唯一缓存文件名称。这个需求主要是配合一些非共用的内容解析。true是，false否(默认)。
            生成唯一名称的目的，是为了每次都要更新，因为文件名称不同每次缓存文件标识也不一样。*/
            [unique] => 
            //【只对html缓存模式有效】标记、签名。目的是用于在同一个模板时，生成不同的编译缓存
   			[sign] => ''
            /*【只对html缓存模式有效】是否添加$_SERVER['REQUEST_URI']参数。true添加(默认)，false不添加。
            只对缓存方式为html有效，添加时，不同路由生成不同缓存页面。*/
            [request_uri] => 1
            //是否获取缓存后的文件。false将直接引入(默认)，true返回缓存文件绝对路径
            [get_cache_file] => 
            /*
            用于检测编译/缓存页面是否存在或者是否需要更新。true是检测模式，false不是检测模式(默认)。
           检测模式，存在返回缓存文件绝对路径，不存在返回false。
           在检测时，配置信息必须要相同。当关闭html缓存的时候，html类型检测返回的总是false。
            get_cache_file和exist对比：
            get_cache_file 这是不存在的时候要进行解析，然后返回缓存文件的绝对路径。
            而exist，存在则返回路径，不存在返回false。当然，两个在一起，exist具有优先级。
            */
            [exist] => 
            //模板解析的时候会，大小写是否不敏感。true不敏感，false敏感(默认)，注意，区分大小写执行速度要快很多。
            [i] => 
            /*
            自定义解析规则，相同的正则表达式会覆盖系统默认的规则。如：
            config('template', array(
            	'compile' => array(
                      '/\{[\s]{0,}#[\s]{0,}\}/' => "<?php /* ",
                      '/\{[\s]{0,}\/[\s]{0,}#[\s]{0,}\}/' => " */ ?>",
                      ......
                ),
            ));
            */
            [compile] => Array
                (
                )
			//是否开启html缓存。true开启(默认)，false关闭。
            [html_cache_start] => 
            //开启缓存情况下，html缓存文件再次自动重新缓存的间隔时间。单位:秒。默认一个小时。
            [html_cache_restart_time] => 3600
            //html、php缓存编译文件自动清理的数量控制，当缓存文件大于该值，将自动清理。0 代表关闭自动清理
            [cache_clean_count] => 1000
            //模板缓存文件保存在运行缓存目录的文件夹名称。
            [cache_folder_name] => template
        )

	/* 数据库配置 */
    [db] => Array
        (
        	//数据库类型
            [type] => mysql
            //主机
            [host] => 127.0.0.1
            //数据库用户名
            [user] => 
            //数据库密码
            [pass] => 
            //数据库名称
            [base] => 
            //端口
            [port] => 3306
            //设置数据库的字符集
            [charset] => utf8
            //表前缀
            [prefix] => 
            //是否打开持久链接
            [persistent] => 
            //初始连接时，数据库不存在则自动创建
   			[base_no_exists_create] => 
            //数据库引擎(该参数对pdo有作用)
            [engine] => mysql
            //数据库缓存文件保存在运行缓存目录的文件夹名称。
            [cache_folder_name] => db
            //是否记录method执行日志信息。true记录(默认)，false不记录。
            [method_log] => 1
            //是否记录query语句执行日志信息。true记录(默认)，false不记录。
            [query_log] => 1
            //单位秒。设置锁的有效时间（时间越长，如果存在死锁的时候很危险）
            [lock_time] => 60
            //是否锁文件超时便显示错误提示并终止程序。默认false不显示不终止只记录
    		[lock_alert] =>
         	//是否锁文件超时便删除。默认true删除。(注意，任何进程都有删除权限。)
            [lock_unlink] => 1
            //是否在 操作事务 回滚的时候 写入日志文件。true记录(默认)，false不记录。
            [work_rollback_log_file] => 1
            
            /*数据库会话模块*/
            [session] => Array (
                //定义初始化的session的id，是一个闭包函数。必须返回字符串
                [id] => 
                //定义失效的时间，是一个闭包函数。必须返回整数
                [expire_time] => Closure Object ()
                //是否开启自动清理，默认true开启，false不开启
                [clear] => 1
                //为true时，如果数据表不存在则自动创建。false不创建
                [found] => 1
                //自动创建数据表时，是否生成日志文件(是一个SQL文件)
                [found_log_file] => 1
                //表结构
                [table] => Array(
                	
                    /* 继承上面设置的表前缀和表字符编码 */
                    
                    //表名称
                    [name] => session
                    //表备注
                    [comment] => 会话信息表
                    //表引擎
                    [engine] => MyISAM
                    //表字段列表
                    [field] => Array(
                        /*接受下面类型字段：*/
        				//id：[必须]创建类型是varchar(155) 主键和唯一键索引
        				//expire_time：[必须]失效的时间字段。创建类型是bigint(20) 普通索引
        				//found_time：[必须]创建时间字段。创建类型是bigint(20) 普通索引
        				//now_time：[必须]当前时间字段。创建类型是bigint(20) 普通索引
        				//var：可变长字符串数据。创建类型是varchar(255) 普通索引
        				//json：json数据。创建类型是text
        				//serialize：序列化数据。创建类型是text
                        //state：状态。创建类型是tinyint(1)
                        [session_id] => id
                        [session_ip] => var
                        [user_id] => var
                        [session_agent] => var
                        [session_json] => json
                        [session_serialize] => serialize
                        [session_expire_time] => expire_time
                        [session_found_time] => found_time
                        [session_now_time] => now_time
                    )
                )
            )
        )

	/* HTTP协议配置 */
    [http] => Array
        (
        	//路由的超文本传输协议，默认为空。https、http
            [protocol] => 
            //协议版本号，默认为空。
            [version] => 
            //主机域名，默认为空。
            [host] => 
            /*URL网址后缀。为0或者false，表示没有后缀，否则添加后缀字符串。
            默认.html，解析出来是http://localhost/index.php/index.html，URL路由模式非0的模式下有效 */
            [suffix] => .html
            //路由界定符、参数分隔符号，[path]参数分隔符。
            [delimiter] => /
            /*URL网址上入口文件名称是否显示。
            默认false不显示，如 http://localhost/index.php/?name=user
            true显示，如 http://localhost/?name=user 
            */
            [index] => 0
    		//子目录链接，字符串类型。默认获取 SUBDIRECTORY_LINK 常量值。
    		[subdirectory_link] => /* SUBDIRECTORY_LINK */
            //定义路径参数。是一个索引数组，如 array('path1','path2','path3');
            [path] => Array
                (
                )
			//设置query参数。是一个关联数组，如 array('key'=>'value', 'key2'=>'value2');
            [query] => Array
                (
                )
			//删除参数键值组。
            [delete] => Array
                (
                	//指定键名，便会被删除。path键名都是整数，如 array(0,3); 
                    [path] => Array
                        (
                        )
					//指定键名，便会被删除。如 array('key','key2');
                    [query] => Array
                        (
                        )

                )
			//锚点
            [anchor] => 
            //开启301重定向(如果该变量不为空，代表开启301重定向，并且值即重定向地址)
            [redirect] => Array
                (
                    [原域名] => 跳转后的域名
                )
			//根据不同的域名，指定不同的配置信息
            [assign] => Array
                (
                    [域名] => Array
                        (
                            [ignore_user_abort] => 1
                            [set_time_limit] => 0
                            ...
                            [error] => Array(...)
                            ...
                        )

                )

        )

	/* 常量信息，只能在 config(NULL) 获取全部配置信息的时候被附带 */
    [get_defined_constants(true)['user']] => Array
        (
            //获取初始化开始的时间 microtime(true) 
            [RUNTIME_START] => 1520200603.0479

            //框架通行证，判断是否从入口文件进入并初始化框架
            [FRAMEWORK_ACCESS] => true 
            //自动获取当前框架类的文件名
            [FRAMEWORK_FILE_NAME] => cao.php 
            //自动获取框架目录名称
            [FRAMEWORK_FOLDER_NAME] => framework 
            //自动获取框架目录的绝对路径(最后没有分隔符)
            [FRAMEWORK_PATH] => C:\WWW\framework 

            //网站根目录的入口文件名称
            [LOCALHOST_FILE_NAME] => index.php 
            //网站根目录的目录名称
            [LOCALHOST_FOLDER_NAME] => localhost 
            //网站根目录的绝对路径(最后没有分隔符)
            [LOCALHOST_PATH] => C:\WWW\localhost 

            //框架所在的服务器目录绝对路径(最后没有分隔符)
            [ROOT_PATH] => C:\WWW 
            //获取缓存目录绝对路径(最后没有分隔符)
            [CACHE_PATH] => C:\WWW\cache 

            //如果程序入口文件不在网站根目录，那么获得子目录(最后没有分隔符)。如果不存在子目录，该值为空。
            [SUBDIRECTORY_LINK] => /dir1/dir2/website 

            //上一页地址
            [HTTP_REFERER] => 
            //HTTP协议版本号
            [HTTP_VERSION] => HTTP/1.1
            //HTTP协议头
			[HTTP_PROTOCOL] => http
            //获得主机域名
            [HTTP_HOST] => cao.php.com 
            //获取访问者真实ip
            [HTTP_IP] => 127.0.0.1 
            //框架版本号 
            [FRAMEWORK_VERSION] => cao.php 0.0.1
        )

)
~~~