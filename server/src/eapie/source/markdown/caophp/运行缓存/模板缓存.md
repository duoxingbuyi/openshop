#### 模板解析后的缓存目录
```
//缓存储存目录生成规则：
CACHE_PATH.
DIRECTORY_SEPARATOR.$config['cache_folder_name'].
DIRECTORY_SEPARATOR

//如：
cache\template
```


#### 不同的解析类型在不同的子目录下：
```
//php编译缓存：
CACHE_PATH.
DIRECTORY_SEPARATOR.$config['cache_folder_name'].
DIRECTORY_SEPARATOR.'php'

如：
cache\template\php\a828d3acc82035bb1239a0858e08aaab.php

//html缓存：
CACHE_PATH.
DIRECTORY_SEPARATOR.$config['cache_folder_name'].
DIRECTORY_SEPARATOR.'html'

如：
cache\template\html\a828d3acc82035bb1239a0858e08aaab.php
```