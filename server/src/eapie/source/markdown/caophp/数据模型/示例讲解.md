#### 创建默认连接
> 第一次使用该标识，并且只有标识没有其他参数，这是使用的是默认配置。


```
//这是获取数据库的默认全局配置
$config_db = config("db");

//建立一个连接，没有传入自定义配置则默认全局配置生效
$db = db('默认');
```

---


#### 自定义局部配置
> 该配置会和全局数据库配置合并，只是当前标识有效。


```
$config = array(
    'base' => 'test2',
    'prefix' => 't_'
);
//初始化一个数据库连接
db('默认', $config);
//再次使用，这是调用这个连接
db('默认');
```


---


#### 覆盖
> 传入一个布尔值，那么就是覆盖这个标识之前的连接，并且初始化新连接。其调试[location]值也会更新。

```
$config = array(
    //......
);
//初始化一个数据库连接
db('默认', $config);
//传入一个布尔值，覆盖旧连接并初始化新连接
db('默认', true);
```

---

#### 创建多个不同的数据库连接
> 同时连接多个不同的数据库，同时操作互不影响。


```
$config1 = array(
'host'=> '127.0.0.1', 
'user'=> 'root',
'pass'=> 'root',
'base'=> 'test'
);
$config2 = array(
'host'=> '127.0.0.1',
'user'=> 'root1',
'pass'=> 'root1',
'base'=> 'test1'
);
$config3 = array(
'host'=> 'aliyun.com',
'user'=> 'root',
'pass'=> 'root',
'base'=> 'test'
);
db('本地数据库1', $config1);
db('本地数据库2', $config2);
db('阿里云数据库', $config3);
```

---

#### 打印调试
> 参数为空，则返回数据模型的调试信息。


```
$config = array(
    'base' => 'test2',
    'prefix' => 't_'
);
//初始化一个数据库连接
db('默认', $config);
//传入一个布尔值，那么就是覆盖这个标识之前的连接，并且初始化新连接
db('默认');
db('测试');

//获取调试信息
$db_list = db();
printexit($db_list);
```

```
/* ******************** 打印结果 ******************** */
Array(
	//操作的类型实例化
    [class] => Array(
    	//所实例化的类名称及详细数据
        [framework\src\db\mysql] => Array(
        	//这是注册的对象
            [register] => framework\src\db\mysql Object()
            //被实例化时的代码位置
            [location] => Z:\WWW\website\localhost\index.php 所在 43 行
            //数据库的连接资源
            [resource] => Array(
            	//下面时每个标识资源的详细信息
                [默认] => Array(
            		/*
            		 运行时间，每次db(标识)都会产生一个单元。
            		 值的单位是秒。记录的当此操作消耗的时间
            		 
            		 比如：
            		 $db1 = db(标识);
            		 db1->......
            		 db1->......
            		 上面属于一次操作，相当于所属于[runtime][0]的操作
            		 又比如：
            		 db(标识)->......
            		 db(标识)->......
            		 上面属于两次操作，相当于所属于[runtime][0]和[runtime][1]的操作
            		 
            		 并且，这里的索引键对应的是method_log和query_log的 [sort]
            		 */
                    [runtime] => Array(
                        [0] => 0
                        [1] => 0
                        [2] => 0
                        )
                    //当前操作步骤的SQL语句拼凑的信息
                    //注意，每次用 db(标识) 那么该信息旧操作信息将会清空
                    [query] => Array(
                    	//数据库信息，里面key是数据库名称，对应是正在操作的数据表名称
                    	//注意，数据表名称是包括整个前缀信息，是一个完整的数据库表名称。
                    	[base] => Array()
                    )
                    //当前标识
                    [id] => 默认
                    //当前连接的配置信息
                    [config] => Array(
                        //......
                        )
                    //连接资源
                    [register] => mysqli Object()
                    //该标识被初始化连接的次数
                    [frequency] => 2
                    //最后一次初始化的代码位置
                    [location] => Z:\WWW\website\localhost\index.php 所在 45 行
                    //方法日志
                    [method_log] => Array()
                    //SQL日志
                    [query_log] => Array()
                    )

                [测试] => Array(
                    [runtime] => Array(
                        [0] => 0
                        )
                    [query] => Array()
                    [id] => 测试
                    [config] => Array(
                    	//......
                        )
                    [register] => mysqli Object()
                    [frequency] => 1
                    [location] => Z:\My\Data\WWW\Framework\cao.php\website\localhost\index.php 所在 48 行
                    [method_log] => Array()
                    [query_log] => Array()
                    )
                    
                )
            )
        )

    //下面是数据库操作类实例化的类名称情况
    [connect] => Array(
            [默认] => Array(
                [class] => framework\src\db\mysql
                )
            [测试] => Array(
                [class] => framework\src\db\mysql
                )
        )
)
```

---

#### 正确操作SQL语句步骤
> 拼凑操作一句SQL语句的正确操作，必须是完整衔接的。可以用对象赋值变量、闭包函数、连贯操作三种方式。


```
//对象赋值变量
$object = db('测试');
$object->table("productdetail l");
$object->select();
//闭包函数
db('测试', function($object){
    $object->table("productdetail l");
    return $object->select();
});
//连贯操作
db('测试')->table("productdetail l")->select();

/*  
这是错误的操作，这不是一个完整的操作步骤
db('测试')->table("productdetail l");
db('测试')->select();//这个时候语句已经更新，而选择的表名称已清洗
*/
```

---

#### 字段映射
> 很多情况下，我们把页面表单中input的name设置为和数据表中字段名一样，方便获取和对数据库进行操作。但是这样存在一些安全隐患，为了避免字段名暴露，我们可以使用字段映射功能。

定义一个字段映射关联数组：

```
$fields_map = array (
	// '表单字段'=>'数据库字段'
);
```
使用命令类库完成映射：

```
//替换key键名称
cmd(array(array $search[,array $replace = array()]), 'arr key_replace');
```
示例：

```
$_POST = array(
    'name' => '猪八戒',
    'Hobby' => '女人',
    'HP' => 100,
    'grade' => '良'
);

$fields_map = array(
    'name' => '姓名',
    'Hobby' => '爱好',
    'grade' => '成绩',
    'effort' => '努力值',
    'HP' => '血量',
);
//闭包函数的方式
//cmd(array($_POST, $fields_map), 'arr key_replace', function($data){
//	printexit($data);
//});
$data = cmd(array($_POST, $fields_map), 'arr key_replace');
printexit($data);
```


```
/* ******************** 打印结果 ******************** */
Array(
    [姓名] => 猪八戒
    [爱好] => 女人
    [血量] => 100
    [成绩] => 良
)
```
