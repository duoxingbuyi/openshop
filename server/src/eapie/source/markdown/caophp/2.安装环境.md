# 运行环境
* PHP 版本要求：PHP5.3+
* 服务器环境，一切可运行php程序系统中。如UNIX、LINUX、WINDOWS、苹果 Macintosh 电脑等平台 。

# 下载源码
~~~
联系作者
~~~

# 伪静态
**Apache**
1. httpd.conf配置文件中加载了mod_rewrite.so模块 
2. AllowOverride None 将None改为 All 
3. 把下面的内容保存为.htaccess文件放到应用入口文件的同级目录下 ：
~~~
<IfModule mod_rewrite.c>
	DirectoryIndex index.html index.php
	RewriteEngine On
	RewriteCond %{REQUEST_FILENAME} !-f
	RewriteCond %{REQUEST_FILENAME} !-d
	RewriteRule ^(.*)$ index.php/$1 [QSA,PT,L]
</IfModule>
~~~
**Nginx**
* 在Nginx低版本中，是不支持PATHINFO的，但是可以通过在Nginx.conf中配置转发规则实现：
~~~
  location / { 
  	// …..省略部分代码   
  	if (!-e $request_filename) {   
  		rewrite  ^(.*)$  /index.php?s=$1  last;   
  		break;    
  		}
  }
~~~
* 如果你的cao.php安装在二级目录，Nginx的伪静态方法设置如下，其中catalogues是所在的目录名称。
~~~
location /catalogues/ {    
	if (!-e $request_filename){        
    	rewrite  ^/catalogues/(.*)$  /catalogues/index.php?s=$1  last;    
        }
 }
~~~
**IIS**
* 如果你的服务器环境支持ISAPI_Rewrite的话，可以配置httpd.ini文件，添加下面的内容：
~~~
RewriteRule (.*)$ /index\.php\?s=$1 [I]
~~~
* 在IIS的高版本下面可以配置web.Config，在中间添加rewrite节点：
~~~
<rewrite> 
<rules> 
<rule name="OrgPage" stopProcessing="true"> 
<match url="^(.*)$" /> 
<conditions logicalGrouping="MatchAll"> 
<add input="{HTTP_HOST}" pattern="^(.*)$" /> 
<add input="{REQUEST_FILENAME}" matchType="IsFile" negate="true" /> 
<add input="{REQUEST_FILENAME}" matchType="IsDirectory" negate="true" /> 
</conditions> 
<action type="Rewrite" url="index.php/{R:1}" /> 
</rule> 
</rules> 
</rewrite>
~~~
