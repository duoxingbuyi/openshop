#### 多条命令用法
多条命令混合使用。注意，第一个命令引入的是第一个数组参数，以此类推。每条命令执行完返回的数据，会成为下一条命令的首个参数。不同的类库用“|”符号隔开。如下：
~~~
$array = array(
    "name"=>"秦始皇",
    "age"=>"未知", 
    "性格"=>array("霸气","义气") ,
    "时代"=>"战国七雄",
    "小名"=>"赵政"
);
$replace = array("name"=>"姓名");
$need = array("姓名", "时代", "小名");
//第三条命令可以省略null，arr和json是两个不同的类库，要用“|”隔开。
$arr = cmd(array($array, $replace), array($need), null, 'arr key_replace whitelist | json encode');
printexit( $arr );
~~~

* * * * *

#### 返回命令列表
不传参数，则返回一个索引数组，是所有的命令列表。
~~~
$arr = cmd();
printexit($arr);
~~~
~~~
/* ******************** 打印结果 ******************** */
Array(
[0] => arr key_replace
[1] => arr whitelist
[2] => arr blacklist
[3] => arr indexedvalue
[4] => disk file_size
[5] => disk info
[6] => disk dir_path
[7] => disk file_path
[8] => href encode
[9] => href decode
[10] => html decode
[11] => html encode
[12] => json decode
[13] => json encode
[14] => random autoincrement
[15] => random ymdhis
[16] => random string
[17] => random number
[18] => random uuid
[19] => str charset
[20] => str addslashes
[21] => str stripslashes
[22] => str toupper
[23] => str tolower
[24] => time date
[25] => time format
[26] => time mktime
[27] => time day_first
[28] => time day_end
[29] => time month_first
[30] => time month_end
[31] => time week_first
[32] => time week_end
[33] => time year_first
[34] => time year_end
[35] => unit storage
[36] => url encode
[37] => url decode
[38] => test\cs test
[39] => test\cs test2
[40] => test\test2\cs2 test
[...] => ...
)
~~~