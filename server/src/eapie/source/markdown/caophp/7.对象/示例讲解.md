
#### 指定标识返回对象
传入已经实例化的标识，以单列模式返回对象：
~~~
//将类名称当作标识
object("application/test", "application/test");
//将标识设为t，实例化application/test类
object("t", "application/test");

//下面是根据不同的标识以单列模式返回不同的实例化对象
$object = object("application/test");
$object = object("t");

//操作这个对象
$object->set_config(); 
object("t")->set_config();
~~~

* * * * *

#### 返回对象列表
参数为空，返回已经被实例化的对象列表信息：
~~~
//返回一个关联数组，如果不存在实例化对象，则返回空数组。
$object = object();
~~~




~~~
/* ******************** 打印结果 ******************** */
Array (
	//application/test 是标识
    [application/test] => Array(
        //这是实例化的对象
        [register] => application\test Object()
        //这是第一次实例化所在代码位置
        [location] => Z:\My\Data\WWW\Framework\cao.php\website\localhost\index.php 所在 35 行
        )
    //application/test_two 是标识
    [application/test_two] => Array(
        [register] => application\test_two Object()
        [location] => Z:WWW\website\localhost\index.php 所在 36 行
        )
    //t 是标识
    [t] => Array(
        [register] => application\test Object( )
        [location] => Z:WWW\website\localhost\index.php 所在 37 行
        )
)
~~~


* * * * *

#### 标识当作类名称
将标识当作类名称来实例化对象。标识和类名称相同，可以省略类名称的方式操作：
~~~
//这是首次实例化application/test类
object("application/test");
object("application/test")->set_config();
~~~

* * * * *

#### 重新实例化
如果标识已经存在并实例化了对象，这个时候可以传入一个为true的布尔值，让这个标识重新实例化对象：
~~~
//首次实例化
object("t", "application/test")；
//覆盖实例化
object("t", "application/test_two"，true)；
//覆盖实例化
object("t", "application/test"，true)；
~~~

在实例化的时候，需要传入构造函数的参数。方式如下：
~~~
object("t", "application/test"，array(123, '哈哈', array('a','b','cd') ));
~~~

* * * * *

#### 构造函数参数和重新实例化
在标识存在，并且已经实例化有对象，而且这个对象有构造函数，这个时候再向构造函数传入参数，那么这个标识将会重新实例化对象，但如果这个对象没有构造函数将无效：
~~~
object("t", "application/test", array(123, '哈哈', array('a','b','cd') ));
//这里只会返回对象
object("t", "application/test");
//将会重新被实例化
object("t", "application/test", array(444, '测试', array('bbb') ));
~~~
~~~
/* ******************** 打印结果 ******************** */
Array(
    [t] => Array(
        [register] => application\test Object(
        	//从这里可看出，对象已经被最后对象操作覆盖了
           [a] => 444
                [b] => 测试
                [c] => Array(
                    [0] => bbb
                    )
            )
        [location] => Z:\WWW\website\localhost\index.php 所在 36 行
        )
)
~~~

* * * * *

#### 复制

有时候需要将已实例化的对象复制给一个新标识。操作方式如下：
~~~
//这里实例化一个对象t
object("t", "application/test",array(123, '哈哈', array('a','b','cd') ));
//t对象复制给h对象
object("h", object("t") );
~~~

~~~
/* ******************** 打印结果 ******************** */
Array(
    [t] => Array(
            [register] => application\test Object(
                [a] => 123
                [b] => 哈哈
                [c] => Array(
                    [0] => a
                    [1] => b
                    [2] => cd
                    )
            )
            [location] => Z:\WWW\website\localhost\index.php 所在 36 行
        )
	
    //可以看出t标识和h标识的对象是一样的，赋值成功
    
    [h] => Array(
            [register] => application\test Object(
                [a] => 123
                [b] => 哈哈
                [c] => Array (
                    [0] => a
                    [1] => b
                    [2] => cd
                    )
                )
            [location] => Z:\WWW\website\localhost\index.php 所在 38 行
        )

)
~~~

* * * * *

#### 闭包函数
用闭包函数方式操作对象。
~~~
//使用闭包函数
object('c',"application/test",function($obj){
	//$obj 等价于 object('c')
    $obj->set_config();
    });
//实例化已经存在对象了
object('c', function($obj){
    $obj->set_config();
});
~~~