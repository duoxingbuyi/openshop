> 判断该常量是否定义，定义则输出常量：


```
{const HTTP_IP}
```

> 模板渲染为：

```
<?php if(defined('HTTP_IP')) echo HTTP_IP; ?>
```
