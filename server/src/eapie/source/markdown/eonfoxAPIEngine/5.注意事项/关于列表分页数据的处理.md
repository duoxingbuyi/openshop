## 分页一般常规的请求参数
|  键   |  名称   |   值类型 |
| --- | --- |--- |
|  search   |   搜索、筛选  |  是一个关联对象，如：{"id":"xxx","state":"1"} |
|  sort |   排序  |  是一个索引数组，_desc表示倒序，_asc表示正序，排在前面的具有优先级。如：["update_time_desc","id_desc","name_asc"] |
|  size |  每页的条数  |  整数，选填 |
|  page |  当前页数 |  整数。如果是等于 all 那么则查询所有 |
|  start |  开始的位置 |  整数。如果存在，则page无效 |



示例：

```
[
    {
        "search": {
            "shop_goods_id": "商品ID",
            "shop_goods_name": "商品名称",
            "shop_goods_state": "商品状态：0|1|2|3",
            "shop_goods_region_province": "省",
            "shop_goods_region_city": "市",
            "shop_goods_region_district": "区",
            "state": "状态",
            "scope": "范围：1|2|3"
        },
        "sort": [
            "shop_goods_name_desc",
            "shop_goods_name_asc",
            "shop_goods_state_desc",
            "shop_goods_state_asc",
            "scope_desc",
            "scope_asc",
            "province_desc",
            "province_asc",
            "city_desc",
            "city_asc",
            "district_desc",
            "district_asc",
            "state_desc",
            "state_asc",
            "insert_time_desc",
            "insert_time_asc",
            "update_time_desc",
            "update_time_asc",
            "sort_desc",
            "sort_asc"
        ],
        "page": "1"
    }
]
```

## 分页数据接口返回值详解与前端序号处理方法
```
{
    "row_count": "0",//数据总条数
    "limit_count": 0,//已取出条数
    "page_size": 10,//每页显示的条数
    "page_count": 0,//总页数
    "page_now": 0,//当前页数
    "data": []//数据
}
```
序号的算法：
```
当前页数-1 * page_size = 已显示的条数
索引键 + 1 + 已显示的条数= 分页后的序号
```
如下模板代码：
```
<tr av-if="list && list.data && list.data.length" av-for="(value, key) list.data" >
<td>{{parseInt(key) + 1 + ((parseInt(list.page_now) - 1) * list.page_size)}}</td>
</tr>
```