<?php
/** ---- eapie ----
 * 优狐积木框架，让开发就像组装积木一样简单！
 * 解放千千万万程序员！这只是1.0版本，后续版本势如破竹！
 * 
 * QQ群：523668865
 * 开源地址 https://gitee.com/lxh888/openshop
 * 官网 http://eonfox.com/
 * 后端框架文档 http://cao.php.eonfox.com
 * 
 * 作者：绵阳市优狐网络科技有限公司
 * 电话/微信：18981181942
 * QQ：294520544
 */



namespace eapie\source\initialize;
class module extends lock {
	
	
	/*模块名称*/
	
	//模块名称
	const 	MODULE_SHOP_GOODS					=   'shop_goods';//商城商品
	const 	MODULE_BRAND						=   'brand';//品牌
	const 	MODULE_HOME							=   'home';//首页模块
	const 	MODULE_HOUSE_PRODUCT				=   'house_product';//楼盘项目
	const	MODULE_SHOP_ORDER					=   'shop_order';//商城订单
	const	MODULE_SHOP_GOODS_TYPE				=   'shop_goods_type';//商城商品分类
	const 	MODULE_BRAND_TYPE					=   'brand_type';//品牌分类
	const 	MODULE_APP_ARTICLE_TYPE				=   'app_article_type';//应用软件文章分类
	const 	MODULE_CMS_ARTICLE_TYPE				=   'cms_article_type';//文章分类
	const 	MODULE_MERCHANT_TYPE				=   'merchant_type';//商家分类
	const 	MODULE_EXPRESS_ORDER_TYPE			=   'express_order_type';//快递订单分类
	const 	MODULE_EXPRESS_ORDER_SHIPPING		=   'express_order_shipping';//快递订单配送
    const 	MODULE_EXPRESS_COUPON				=   'express_coupon';//快递优惠券模块
	const   MODULE_MERCHANT_GOODS_TYPE          =   'merchant_goods_type';//商家商品分类
	const	MODULE_MERCHANT						=	'merchant';//商家
	
	
	
	
	
}
?>