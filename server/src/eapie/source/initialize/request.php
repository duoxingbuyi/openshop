<?php
/** ---- eapie ----
 * 优狐积木框架，让开发就像组装积木一样简单！
 * 解放千千万万程序员！这只是1.0版本，后续版本势如破竹！
 * 
 * QQ群：523668865
 * 开源地址 https://gitee.com/lxh888/openshop
 * 官网 http://eonfox.com/
 * 后端框架文档 http://cao.php.eonfox.com
 * 
 * 作者：绵阳市优狐网络科技有限公司
 * 电话/微信：18981181942
 * QQ：294520544
 */



namespace eapie\source\initialize;
class request extends project {
	
	
	
	/*请求*/
	
	//请求
	const 	REQUEST_ADMINISTRATOR	=	'eapie\source\request\administrator';
	const 	REQUEST_API				=	'eapie\source\request\api';
	const 	REQUEST_APPLICATION		=	'eapie\source\request\application';
	const 	REQUEST_SESSION			=	'eapie\source\request\session';
	const 	REQUEST_USER			=	'eapie\source\request\user';
	const 	REQUEST_ADMIN			=	'eapie\source\request\admin';
	const 	REQUEST_SOFTSTORE		=	'eapie\source\request\softstore';
	const 	REQUEST_TEST			=	'eapie\source\request\test';
	const 	REQUEST_SHOP			=	'eapie\source\request\shop';
	const	REQUEST_MERCHANT		=	'eapie\source\request\merchant';
	const	REQUEST_PROJECT			=	'eapie\source\request\project';
	const	REQUEST_CMS				=	'eapie\source\request\cms';
	
	/*代理系统*/
	const	REQUEST_AGENT			=	'eapie\source\request\agent';
	
	/*快递系统*/
	const	REQUEST_EXPRESS			=	'eapie\source\request\express';
	
	
	
	
}
?>